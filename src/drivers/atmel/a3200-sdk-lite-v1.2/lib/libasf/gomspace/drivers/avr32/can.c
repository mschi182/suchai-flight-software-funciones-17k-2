/*
Cubesat Space Protocol - A small network-layer protocol designed for Cubesats
Copyright (C) 2012 GomSpace ApS (http://www.gomspace.com)
Copyright (C) 2012 AAUSAT3 Project (http://aausat3.space.aau.dk)

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
*/

/* AVR32UC3C0512C driver */

#include <stdio.h>
#include <string.h>

#include <sysclk.h>
#include <gpio.h>
#include <status_codes.h>
#include <delay.h>

#include <util/error.h>
#include <util/csp_buffer.h>
#include <dev/can.h>

#include <FreeRTOS.h>

#include <canif.h>

#include <util/log.h>
LOG_GROUP_MASKED(log_can, "can", LOG_INFO_MASK|LOG_WARNING_MASK|LOG_ERROR_MASK, 0);
#define LOG_DEFAULT log_can

volatile can_msg_t mob_ram_ch[2][NB_MOB_CHANNEL] __attribute__ ((section (".hsb_ram_loc")));

/* Todo: Should this be configurable compile time, run time or not at all? */
static uint8_t can_channel = 0; /* 2 channels available, 0 or 1 */

static uint8_t can_operating_mode = CANIF_CHANNEL_MODE_NORMAL;

/** Callback functions */
can_tx_callback_t txcb = NULL;
can_rx_callback_t rxcb = NULL;

/** Identifier and mask */
uint32_t can_id;
uint32_t can_mask;

/** ISR prototype */
static void can_int_tx_handler(void);
static void can_int_rx_handler(void);
static void can_int_busoff_handler(void);
static void can_int_error_handler(void);
static void can_int_wakeup_handler(void);

#define CAN_CMD_REFUSED          0xFF
#define CAN_CMD_ACCEPTED         0x00
#define CAN_STATUS_COMPLETED     0x00
#define CAN_STATUS_NOT_COMPLETED 0x01
#define CAN_STATUS_ERROR         0x02
#define CAN_STATUS_WAKEUP        0x03
#define CAN_STATUS_BUSOFF        0x04
#define CAN_MOB_NOT_ALLOCATED    0xFF
#define CAN_DATA_FRAME              0
#define CAN_REMOTE_FRAME           1

static uint32_t can_mob_alloc_vector[2] = {0, 0};
static uint8_t no_rx_mob = NB_MOB_CHANNEL / 2;

uint8_t can_mob_alloc(uint8_t ch, uint8_t offset)
{
	if ((ch > 1)) {
		return  CAN_CMD_REFUSED;
	}
	for (uint8_t mob = offset; mob < NB_MOB_CHANNEL; mob++)
	{
		if (!((can_mob_alloc_vector[ch] >> mob) & 0x01))
		{
			can_mob_alloc_vector[ch] |= (1 << mob);
			CANIF_clr_mob(ch, mob);
			return mob;
		}
	}
	return CAN_CMD_REFUSED;
}

uint8_t can_mob_free(uint8_t ch, uint8_t mob)
{
	if ((ch > 1) || (mob > (NB_MOB_CHANNEL - 1))) {
		return CAN_CMD_REFUSED;
	}
	can_mob_alloc_vector[ch] &= (~(1 << mob));
	return CAN_CMD_ACCEPTED;
}

/** Setup CAN interrupts */
static void can_init_interrupt(uint32_t id, uint32_t mask) {

	if (can_channel == 0) {
		INTC_register_interrupt(&can_int_tx_handler, AVR32_CANIF_TXOK_IRQ_0, CAN0_INT_TX_LEVEL);
		INTC_register_interrupt(&can_int_rx_handler, AVR32_CANIF_RXOK_IRQ_0, CAN0_INT_RX_LEVEL);
		INTC_register_interrupt(&can_int_busoff_handler, AVR32_CANIF_BUS_OFF_IRQ_0, CAN0_INT_BOFF_LEVEL);
		INTC_register_interrupt(&can_int_error_handler, AVR32_CANIF_ERROR_IRQ_0, CAN0_INT_ERR_LEVEL);
		INTC_register_interrupt(&can_int_wakeup_handler, AVR32_CANIF_WAKE_UP_IRQ_0, CAN0_INT_WAKE_UP_LEVEL);
		CANIF_enable_interrupt(can_channel);
	} else if (can_channel == 1) {
		INTC_register_interrupt(&can_int_tx_handler, AVR32_CANIF_TXOK_IRQ_1, CAN1_INT_TX_LEVEL);
		INTC_register_interrupt(&can_int_rx_handler, AVR32_CANIF_RXOK_IRQ_1, CAN1_INT_RX_LEVEL);
		INTC_register_interrupt(&can_int_busoff_handler, AVR32_CANIF_BUS_OFF_IRQ_1, CAN1_INT_BOFF_LEVEL);
		INTC_register_interrupt(&can_int_error_handler, AVR32_CANIF_ERROR_IRQ_1, CAN1_INT_ERR_LEVEL);
		INTC_register_interrupt(&can_int_wakeup_handler, AVR32_CANIF_WAKE_UP_IRQ_1, CAN1_INT_WAKE_UP_LEVEL);
		CANIF_enable_interrupt(can_channel);
	}
}

int can_init(uint32_t id, uint32_t mask, can_tx_callback_t atxcb, can_rx_callback_t arxcb, can_config_t *conf) {

	can_config_t my_conf;

	log_debug("can_init(): id=0x%08lX, mask=0x%08lX, conf=%lX", id, mask, (unsigned long)conf);
	if (can_channel > 1) {
		log_error("Error: Illegal channel index: %u", can_channel);
		return 0;
	}
	if (conf == NULL) {
		my_conf.bitrate = 500000;
		my_conf.clock_speed = 16000000;
		conf = &my_conf;
		log_error("Error: Missing can config, setting to defaults: %lu %lu", conf->bitrate, conf->clock_speed);
	}
	if (!conf->bitrate || !conf->clock_speed) {
		log_error("Error: Illegal bitrate and/or clock_speed: %lu %lu", conf->bitrate, conf->clock_speed);
		conf->clock_speed = 16000000;
		conf->bitrate = 500000;
	}
	/* Set can_id and can_mask from id and mask arguments */
	can_id = id & 0x1FFFFFFF;
	can_mask = mask & 0x1FFFFFFF;

	/* Set callbacks */
	txcb = atxcb;
	rxcb = arxcb;

	/* Initialize CAN channel */
	CANIF_set_reset(can_channel);
	uint8_t breaker = 0;
	while (CANIF_channel_enable_status(can_channel)) {
		if (breaker > 10) {
			log_error("Error: Enable status did not reach 0 within 100 us");
			return 0;
		}
		delay_us(10);
		breaker++;
	}
	CANIF_clr_reset(can_channel);

	/* Set MOb working memory */
	CANIF_set_ram_add(can_channel,(unsigned long)&mob_ram_ch[can_channel][0]);
	/* Init bit timing */
	if ((CANIF_bit_timing(can_channel)) == 0) {
		return (0);
	}
	/* Tbit = (PRS + PHS1 + PHS2 + 4) x (PRES + 1) x PGCLK_CANIF
	 * where PGCLK_CANIF is the same as OSC0 frequency */
	uint32_t pres = CANIF_get_pres(can_channel);
	switch (conf->bitrate)
	{
	case 1000000:
		pres = 1;
		break;
	case 500000:
		pres = 3;
		break;
	case 250000:
		pres = 7;
		break;
	default:
		/* Default is 500000 */
		pres = 3;
	}
	CANIF_set_pres(can_channel, pres);

	pres = CANIF_get_pres(can_channel);
	uint32_t phs1 = CANIF_get_phs1(can_channel);
	uint32_t phs2 = CANIF_get_phs2(can_channel);
	uint32_t prs = CANIF_get_prs(can_channel);
	uint32_t bit_rate = BOARD_OSC0_HZ / ((prs + phs1 + phs2 + 4) * (pres + 1));
	log_info("can_init: bit_rate: %lu osc: %d phs1:%lu phs2:%lu pres:%lu prs:%lu sjw:%lu sm:%lu",
			bit_rate, BOARD_OSC0_HZ, phs1, phs2, pres, prs, CANIF_get_sjw(can_channel), CANIF_get_sm(can_channel));
	if ((bit_rate > 1000000) || (bit_rate < 250000)) {
		log_warning("Non-standard CAN bit rate: %"PRIu32" mbps", bit_rate);
	}
	/* Todo: Fix overrun setting */
	can_operating_mode = CANIF_CHANNEL_MODE_NORMAL;
	switch (can_operating_mode)
	{
	case CANIF_CHANNEL_MODE_NORMAL:
		CANIF_set_channel_mode(can_channel, 0);
		CANIF_clr_overrun_mode(can_channel);
		break;
	case CANIF_CHANNEL_MODE_LISTENING:
		CANIF_set_channel_mode(can_channel, 1);
		CANIF_set_overrun_mode(can_channel);
		break;
	case CANIF_CHANNEL_MODE_LOOPBACK:
		CANIF_set_channel_mode(can_channel, 2);
		CANIF_clr_overrun_mode(can_channel);
		break;
	}
	/* Initialise all MOb's and enable can channel */
	canif_clear_all_mob(can_channel, NB_MOB_CHANNEL);
	CANIF_enable(can_channel);

	/*
	 * The maximum delay time to wait is the time to transmit 128-bits
	 * (CAN extended frame at baudrate speed configured by the user).
	 * - 10x bits number of previous undetected message,
	 * - 128x bits MAX length,
	 * - 3x bits of interframe.
	 */
#define DELAY_HZ         (BAUDRATE_HZ / 141.0)   /*Compute Maximum delay time*/
#define DELAY            (1000000 / DELAY_HZ)  /*Compute Delay in us*/
	delay_us(DELAY);
	if (!CANIF_channel_enable_status(can_channel)) {
		log_error("Error: Enable status did not reach 1 within %f us", DELAY);
		return 0;
	}

	/* Enable interrupts */
	can_init_interrupt(id, mask);
	/* Enable all error conditions */
	AVR32_CANIF.channel[can_channel].canier |= (
			AVR32_CANIF_CANIER_AERRIM_MASK |
			AVR32_CANIF_CANIER_BERRIM_MASK |
			AVR32_CANIF_CANIER_CERRIM_MASK |
			AVR32_CANIF_CANIER_FERRIM_MASK |
			AVR32_CANIF_CANIER_SERRIM_MASK);

	log_debug("can_init(): canisr: %08lX cansr: %08lX cancfg: %08lX canctrl: %08lX canfc: %08lX canimr: %08lX",
			AVR32_CANIF.channel[can_channel].canisr, AVR32_CANIF.channel[can_channel].cansr,
			AVR32_CANIF.channel[can_channel].cancfg, AVR32_CANIF.channel[can_channel].canctrl,
			AVR32_CANIF.channel[can_channel].canfc, AVR32_CANIF.channel[can_channel].canimr);

	/* Allocate and configure half of available MOb's for RX (0 - N/2-1) */
	for (uint8_t i = 0; i < no_rx_mob; i++) {
		uint8_t mob = can_mob_alloc(can_channel, i);
		CANIF_set_ext_id(can_channel, mob, can_id);
		CANIF_set_ext_idmask(can_channel, mob, can_mask);
		CANIF_mob_get_ptr_data(can_channel, mob)->ide_mask_bit = 0;
		CANIF_clr_rtrmask(can_channel, mob);
		CANIF_mob_clr_automode(can_channel, mob);
		CANIF_mob_clr_dlc(can_channel, mob);
		CANIF_config_rx(can_channel, mob);
		CANIF_mob_enable(can_channel, mob);
		CANIF_mob_enable_interrupt(can_channel, mob);
	}
	log_debug("can_init(): done (breaker: %u)", breaker);

	return 0;

}

int can_send(can_id_t id, uint8_t data[], uint8_t dlc, void * task_woken) {

	if (task_woken == NULL) {
		portENTER_CRITICAL();
	}
	uint8_t mob = can_mob_alloc(can_channel, no_rx_mob);
	if (task_woken == NULL) {
		portEXIT_CRITICAL();
	}
	if (mob == CAN_CMD_REFUSED) {
		/* No free MOb available */
		log_error("TX overflow, no available MOB");
		return -1;
	}
	/* Set ext id, dlc and copy data to MOb */
	CANIF_set_ext_id(can_channel, mob, id);
	CANIF_set_ext_idmask(can_channel, mob, 0);
	CANIF_mob_clr_dlc(can_channel, mob);
	CANIF_mob_set_dlc(can_channel, mob, dlc);
	memcpy((CANIF_mob_get_ptr_data(can_channel, mob))->data.u8, data, dlc);
	/* Configure MOb for TX */
	CANIF_config_tx(can_channel, mob);
	/* Enable MOb and interrupt to trigger TX */
	CANIF_mob_enable(can_channel, mob);
	CANIF_mob_enable_interrupt(can_channel, mob);

	return 0;

}

__attribute__((__interrupt__))
static void can_int_tx_handler(void)
{
	/* Get the first MOb that was transmitted */
	uint8_t mob = CANIF_mob_get_mob_txok(can_channel);
	if (mob != 0x20)
	{
		/* Clear TX OK and reset MOb status to ack interrupt */
		CANIF_mob_clear_txok_status(can_channel, mob);
		CANIF_mob_clear_status(can_channel, mob);
		/* Get ext id from MOb */
		can_id_t id = CANIF_get_ext_id(can_channel, mob);
		can_mob_free(can_channel, mob);
		/* Call TX Callback with no error */
		portBASE_TYPE task_woken = pdFALSE;
		if (txcb != NULL) {
			txcb(id, CAN_NO_ERROR, &task_woken);
		}
	}
}

__attribute__((__interrupt__))
static void can_int_rx_handler(void)
{
	/* Get the first MOb received */
	uint8_t mob = CANIF_mob_get_mob_rxok(can_channel);
	if (mob != 0x20) {
		/* Clear RX OK and reset MOb status to ack interrupt */
		CANIF_mob_clear_rxok_status(can_channel, mob);
		CANIF_mob_clear_status(can_channel, mob);
		/* Extract ext id, dlc and data from can MOb frame */
		can_frame_t frame;
		frame.id = CANIF_get_ext_id(can_channel, mob);
		frame.dlc = CANIF_mob_get_dlc(can_channel, mob);
		can_msg_t *cm = CANIF_mob_get_ptr_data(can_channel, mob);
		frame.data32[0] = cm->data.u32[0];
		frame.data32[1] = cm->data.u32[1];
		/* Re-enable MOb to enable reception of new MOb */
		CANIF_mob_enable(can_channel, mob);
		CANIF_mob_enable_interrupt(can_channel, mob);
		/* Call RX Callback */
		portBASE_TYPE task_woken = pdFALSE;
		if (rxcb != NULL) {
			rxcb(&frame, &task_woken);
		}
	}
}

__attribute__((__interrupt__))
static void can_int_busoff_handler(void) {
	uint8_t mob = CANIF_get_interrupt_lastmob_selected(can_channel);
	if (mob != 0x20)
	{
		if (CANIF_mob_get_dir(can_channel, mob) == 1) {
			/* Clear TX OK and reset MOb status to ack interrupt */
			CANIF_mob_clear_txok_status(can_channel, mob);
			CANIF_mob_clear_status(can_channel, mob);
			/* Get ext id from MOb */
			can_id_t id = CANIF_get_ext_id(can_channel, mob);
			CANIF_mob_disable(can_channel, mob);
			can_mob_free(can_channel, mob);
			/* Call TX Callback with no error */
			portBASE_TYPE task_woken = pdFALSE;
			if (txcb != NULL) {
				txcb(id, CAN_ERROR, &task_woken);
			}
		} else {
			mob = 0x20;
		}
	}
	CANIF_clr_interrupt_status(can_channel);
}

/*
 * From avrfreaks.net:
 * The CANSTMOB AERR (Acknowledge error) is unique (according to the Bosch
 * CAN specification). Any other CAN Tx error will increment CANTEC up to
 * the bus off error confinement state (255 then an overflow).
 * However, AERR all by itself will not increment CANTEC after it reaches
 * error passive (128). When the CANSTMOB RXOK and TXOK flags are set,
 * the CAN hardware will clear all the BERR, SERR, CERR, FERR and AERR
 * error flags. All of these CANSTMOB error flag values are temporary unless
 * your Tx never works at all. If you do get a TXOK flag, then the Tx did work.
 * Since AERR all by itself never triggers bus off error confinement,
 * it just keeps sending Tx retries forever until a TXOK or until your
 * software shuts it down (except for TTC mode).
 * The Tx AERR usually means your CAN node is disconnected from the CAN network,
 * which probably explains why Bosch did not allow it to trigger a bus off.
 */

__attribute__((__interrupt__))
static void can_int_error_handler(void) {
	uint8_t mob = CANIF_get_interrupt_lastmob_selected(can_channel);
	if (mob != 0x20)
	{
		if (CANIF_mob_get_dir(can_channel, mob) == 1) {
			/* Clear TX OK and reset MOb status to ack interrupt */
			CANIF_mob_clear_txok_status(can_channel, mob);
			CANIF_mob_clear_status(can_channel, mob);
			/* Get ext id from MOb */
			can_id_t id = CANIF_get_ext_id(can_channel, mob);
			CANIF_mob_disable(can_channel, mob);
			can_mob_free(can_channel, mob);
			/* Call TX Callback with no error */
			portBASE_TYPE task_woken = pdFALSE;
			if (txcb != NULL) {
				txcb(id, CAN_ERROR, &task_woken);
			}
		} else {
			mob = 0x20;
		}
	}
	CANIF_clr_interrupt_status(can_channel);
}

__attribute__((__interrupt__))
static void can_int_wakeup_handler(void) {
	CANIF_clr_interrupt_status(can_channel);
}
