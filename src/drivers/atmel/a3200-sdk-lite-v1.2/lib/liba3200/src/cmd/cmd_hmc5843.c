/**
 * cmd_periph.c
 *
 *  Created on: 16/12/2009
 */


#include "hmc5843.h"

#include <string.h>
#include <stdio.h>
#include <FreeRTOS.h>
#include <task.h>
#include <util/console.h>
#include <command/command.h>
#include <dev/usart.h>
#include <dev/i2c.h>

#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include <util/error.h>
#include <util/test_malloc.h>

#include <conf_asf.h>

typedef struct hmc5843_reg_data_s {
	uint8_t id1;
	uint8_t id2;
	uint8_t id3;
	uint8_t conf_a;
	uint8_t conf_b;
	uint8_t mode;
	uint16_t x;
	uint16_t y;
	uint16_t z;
	uint8_t status;
} hmc5843_reg_data_t;

/** Default values */
static float scale = 1/1.3;

int cmd_hmc5843_setgain(struct command_context *ctx) {

	char * args = command_args(ctx);
	unsigned int gain;
	if (sscanf(args, "%u", &gain ) != 1)
	  return CMD_ERROR_SYNTAX;

	if (gain > 7)
	  return CMD_ERROR_SYNTAX;

	if (hmc5843_set_conf(hmc5843_get_rate(), hmc5843_get_meas(), (hmc5843_gain_t)gain) == E_NO_ERR)
		return CMD_ERROR_NONE;
	else
		return CMD_ERROR_FAIL;
}

int cmd_hmc5843_setrate(struct command_context *ctx) {

	char * args = command_args(ctx);
	unsigned int rate;
	if (sscanf(args, "%u", &rate ) != 1)
	  return CMD_ERROR_SYNTAX;

	if (rate > 6)
	  return CMD_ERROR_SYNTAX;

	if (hmc5843_set_conf((hmc5843_rate_t)rate, hmc5843_get_meas(), hmc5843_get_gain()) == E_NO_ERR)
		return CMD_ERROR_NONE;
	else
		return CMD_ERROR_FAIL;
}


int cmd_hmc5843_init(struct command_context *ctx) {
	if (hmc5843_init_force())
		return CMD_ERROR_NONE;
	else
		return CMD_ERROR_FAIL;
}

// Read in entire status of hmc5843
int hmc5843_get_info(struct command_context *ctx) {
	uint8_t txdata[1];
	uint8_t rxdata[13];
	txdata[0] = 0x0A;

 	int ret;
	ret = hmc5843_init();
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;
	

	ret = i2c_master_transaction(2, 0x1E, txdata, 1, rxdata, 13, 500);
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;


	hmc5843_reg_data_t * data = (void *) rxdata;

	printf("ID: %c%c%c\r\n", data->id1, data->id2, data->id3);

	uint8_t dorate = (data->conf_a >> 2) & 0x07;
	float dorate_f = 0;
	switch (dorate) {
	case 0: dorate_f = 0.5; break;
	case 1: dorate_f = 1; break;
	case 2: dorate_f = 2; break;
	case 3: dorate_f = 5; break;
	case 4: dorate_f = 10; break;
	case 5: dorate_f = 20; break;
	case 6: dorate_f = 50; break;
	case 7: dorate_f = 0; break;
	}
	printf("Data output rate is %f [Hz]\r\n", dorate_f);

	uint8_t mmode = (data->conf_a & 0x3);
	switch (mmode) {
	case 0: printf("Normal Measurement Mode\r\n"); break;
	case 1: printf("Positive Bias Mode\r\n"); break;
	case 2: printf("Negative Bias Mode\r\n"); break;
	case 3: printf("Invalid mode\r\n"); break;
	}

	uint8_t gain = ((data->conf_b >> 5) & 0x7);
	switch (gain) {
	case 0: printf("Field Range: +- 0.7 [Ga] Gain 1620\r\n"); break;
	case 1: printf("Field Range: +- 1.0 [Ga] Gain 1300\r\n"); break;
	case 2: printf("Field Range: +- 1.5 [Ga] Gain 970\r\n"); break;
	case 3: printf("Field Range: +- 2.0 [Ga] Gain 780\r\n"); break;
	case 4: printf("Field Range: +- 3.2 [Ga] Gain 530\r\n"); break;
	case 5: printf("Field Range: +- 3.8 [Ga] Gain 460\r\n"); break;
	case 6: printf("Field Range: +- 4.5 [Ga] Gain 390\r\n"); break;
	case 7: printf("Field Range: +- 6.5 [Ga] Gain 280 (Not recommended!)\r\n"); break;
	}
	printf("Scale %f\r\n",scale);

	uint8_t mode = data->mode & 3;
	switch (mode) {
	case 0: printf("Continuous-Conversion Mode\r\n"); break;
	case 1: printf("Single-Conversion Mode\r\n"); break;
	case 2: printf("Idle Mode\r\n"); break;
	case 3: printf("Sleep Mode\r\n"); break;
	}

	return CMD_ERROR_NONE;

}



// Do single conversion and print it
int hmc5843_test_single(struct command_context *ctx) {

	hmc5843_data_t data;
	int ret;
	ret = hmc5843_init();
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;

	if (hmc5843_read_test(&data) == E_NO_ERR) {
		printf("X: %4.1f mG\n\r", data.x);
		printf("Y: %4.1f mG\n\r", data.y);
		printf("Z: %4.1f mG\n\r", data.z);
		printf("Magnitude: %4.1f mG\r\n", sqrt(powf(data.x,	2.0) + powf(data.y, 2.0) + powf(data.z, 2.0)));
	}
	else {
		printf("Error reading from magnetometer\r\n");
		return CMD_ERROR_FAIL;
	}

	return CMD_ERROR_NONE;
}

// Do bias testing
int hmc5843_test_bias(struct command_context *ctx) {

	int ret;
	ret = hmc5843_init();
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;

	hmc5843_set_mode(MAG_MODE_SINGLE);
	hmc5843_set_conf(hmc5843_get_rate(), MAG_MEAS_POS, hmc5843_get_gain() );

	printf("Positive bias mode\r\n");
	vTaskDelay(configTICK_RATE_HZ * 0.1);

	hmc5843_test_single(NULL);
	hmc5843_test_single(NULL);
	hmc5843_test_single(NULL);

	hmc5843_set_conf(hmc5843_get_rate(), MAG_MEAS_NEG, hmc5843_get_gain() );

	printf("Negative bias mode\r\n");
	vTaskDelay(configTICK_RATE_HZ * 0.1);

	hmc5843_test_single(NULL);
	hmc5843_test_single(NULL);
	hmc5843_test_single(NULL);

	hmc5843_set_conf(hmc5843_get_rate(), MAG_MEAS_NORM, hmc5843_get_gain() );

	printf("Normal bias mode\r\n");
	vTaskDelay(configTICK_RATE_HZ * 0.1);

	hmc5843_test_single(NULL);
	hmc5843_test_single(NULL);
	hmc5843_test_single(NULL);

	hmc5843_set_mode(MAG_MODE_IDLE);

	return CMD_ERROR_NONE;
}



// Do loop measurements
int hmc5843_loop(struct command_context *ctx) {

	hmc5843_data_t data;
	int ret;
	ret = hmc5843_init();
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;

	hmc5843_set_mode(MAG_MODE_CONTINUOUS);

	while (1) {

		if (usart_messages_waiting(USART_CONSOLE) != 0)
			break;

		if (hmc5843_read(&data) == E_NO_ERR) {
			console_clear();
			printf("X: %4.1f mG\n\r", data.x);
			printf("Y: %4.1f mG\n\r", data.y);
			printf("Z: %4.1f mG\n\r", data.z);
			printf("Magnitude: %4.1f mG\n\r", sqrt(powf(data.x,	2.0) + powf(data.y, 2.0) + powf(data.z, 2.0)));
		}

		vTaskDelay(configTICK_RATE_HZ * 0.100);
	}

	hmc5843_set_mode(MAG_MODE_IDLE);

	return CMD_ERROR_NONE;
}


// Do loop measurements
int hmc5843_loop_noformat(struct command_context *ctx) {

	hmc5843_data_t data;
	int ret;
	ret = hmc5843_init();
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;

	ret = hmc5843_set_mode(MAG_MODE_CONTINUOUS);
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;
;

	while (1) {

		if (usart_messages_waiting(USART_CONSOLE) != 0)
			break;

		if (hmc5843_read(&data) == E_NO_ERR) {
			printf(" %4.1f, ", data.x);
			printf(" %4.1f, ", data.y);
			printf(" %4.1f, ", data.z);
			printf(" %4.1f \n\r", sqrt(powf(data.x,	2.0) + powf(data.y, 2.0) + powf(data.z, 2.0)));
		}

		vTaskDelay(configTICK_RATE_HZ * 0.100);

	}

	hmc5843_set_mode(MAG_MODE_IDLE);

	return CMD_ERROR_NONE;
}


// Do loop measurements (raw values)
int hmc5843_loop_raw(struct command_context *ctx) {

	hmc5843_data_t data;
	int ret;
	ret = hmc5843_init();
	if (ret != E_NO_ERR)
		return CMD_ERROR_FAIL;

	hmc5843_set_mode(MAG_MODE_CONTINUOUS);

	while (1) {

		if (usart_messages_waiting(USART_CONSOLE) != 0)
			break;

		if (hmc5843_read_raw(&data) == E_NO_ERR) {
			printf(" %4.1f, ", data.x);
			printf(" %4.1f, ", data.y);
			printf(" %4.1f, ", data.z);
			printf(" %4.1f \n\r", sqrt(powf(data.x,	2.0) + powf(data.y, 2.0) + powf(data.z, 2.0)));
		}

		vTaskDelay(configTICK_RATE_HZ * 0.100);

	}

	hmc5843_set_mode(MAG_MODE_IDLE);

	return CMD_ERROR_NONE;
}


struct command hmc5843_subcommands[] = {
	{
		.name = "read",
		.help = "Magnetometer read",
		.handler = hmc5843_test_single,
	},{
		.name = "loop",
		.help = "Magnetometer read in a loop",
		.handler = hmc5843_loop,
	},{
		.name = "init",
		.help = "Magnetometer init",
		.handler = cmd_hmc5843_init,
	},{
		.name = "loop_noformat",
		.help = "Magnetometer read in a loop (not formatted)",
		.handler = hmc5843_loop_noformat,
	},{
		.name = "loop_raw",
		.help = "Magnetometer read in a loop (raw output)",
		.handler = hmc5843_loop_raw,
	},{
		.name = "status",
		.help = "Read magnetometer status registers",
		.handler = hmc5843_get_info,
	},{
		.name = "bias",
		.help = "Test magnetometer bias",
		.handler = hmc5843_test_bias,
	},{
		.name = "setgain",
		.help = "Set magnetometer gain",
		.usage = "GAIN0.7 = 0  ... GAIN6.5 = 7",
		.handler = cmd_hmc5843_setgain,
	},{
		.name = "setrate",
		.help = "Set magnetometer rate",
		.usage = "RATE0.5 = 0  ... RATE50 = 6",
		.handler = cmd_hmc5843_setrate,
	}
};

struct command __root_command hmc5843_commands[] = {
	{
		.name = "hmc5843",
		.help = "HMC5843 commands",
		.chain = INIT_CHAIN(hmc5843_subcommands),
	}
};

void cmd_hmc5843_setup(void) {
	command_register(hmc5843_commands);
}

