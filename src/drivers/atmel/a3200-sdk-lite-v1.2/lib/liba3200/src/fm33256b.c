/**
 * @file fm33256b.c
 * Serial driver for the Maxim FM33256B RTC
 *
 * @note Data is transmitted LSB first. Write data is
 * sampled on the rising edge of SCLK. Read data is
 * output on the falling edge of SCLK.
 *
 * @author Jeppe Ledet-Pedersen
 * @author Morten Jensen	 
 * Copyright 2013 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include <spi.h>
#include <gpio.h>
#include <sysclk.h>

#include <util/delay.h>
#include <util/log.h>
#include <util/timestamp.h>

#include <FreeRTOS.h>
#include <semphr.h>

#include "fm33256b.h"
#include "board.h"

static SemaphoreHandle_t fm33256b_lock = NULL;

#define FM33256B_BIT_DELAY_US		5

/* Convert from BCD values to something sane */
static inline uint8_t bcd_to_sane(uint8_t bcd) {
	return (bcd >> 4) * 10 + (bcd & 0x0F);
}

static inline uint8_t sane_to_bcd(uint8_t sane) {
	return ((sane / 10) << 4) + (sane % 10);
}

static inline uint8_t reverse_bits(uint8_t bits) {
	unsigned int b = bits;
	b = ((b * 0x0802LU & 0x22110LU) | (b * 0x8020LU & 0x88440LU)) * 0x10101LU >> 16;
	return (uint8_t) b;
}

static void lock(void) {
	if (fm33256b_lock == NULL)
		return;
	if (xSemaphoreTake(fm33256b_lock, 1 * configTICK_RATE_HZ) == pdFALSE)
		log_error("FRAM Lock failed");
}

static void unlock(void) {
	if (fm33256b_lock == NULL)
		return;
	xSemaphoreGive(fm33256b_lock);
}

int fm33256b_write(uint8_t *in, int inlen) {

	/* Enable writes */
	spi_selectChip(FM33256_SPI, FM33256_SPI_CS);
	spi_write(FM33256_SPI, FM33_WREN);
	spi_unselectChip(FM33256_SPI, FM33256_SPI_CS);

	/* Write data */
	spi_selectChip(FM33256_SPI, FM33256_SPI_CS);
	for(int i = 0; i < inlen; i++) {
		spi_write(FM33256_SPI, in[i]);
	}
	spi_unselectChip(FM33256_SPI, FM33256_SPI_CS);
	return 0;
}

int fm33256b_read(uint8_t *cmd, uint8_t cmdlen, uint8_t *out, int outlen) {
	spi_selectChip(FM33256_SPI, FM33256_SPI_CS);
	for(int i = 0; i < cmdlen; i++) {
		spi_write(FM33256_SPI, cmd[i]);
	}
	for(int i = 0; i < outlen; i++) {
		uint16_t read;
		spi_write(FM33256_SPI, 0xff);
		spi_read(FM33256_SPI, &read);
		out[i] = (uint8_t) read;
	}
	spi_unselectChip(FM33256_SPI, FM33256_SPI_CS);
	return 0;
}

void fm33256b_write_data(uint16_t addr, uint8_t *data, uint16_t len) {
	lock();

	/* Enable writes */
	spi_selectChip(FM33256_SPI, FM33256_SPI_CS);
	spi_write(FM33256_SPI, FM33_WREN);
	spi_unselectChip(FM33256_SPI, FM33256_SPI_CS);

	/* Write data */
	spi_selectChip(FM33256_SPI, FM33256_SPI_CS);
	uint8_t cmd[3] = {FM33_WRITE, addr >> 8, addr & 0xff};
	for(int i = 0; i < 3; i++)
			spi_write(FM33256_SPI, cmd[i]);

	for(int i = 0; i < len; i++) {
		spi_write(FM33256_SPI, data[i]);
	}
	spi_unselectChip(FM33256_SPI, FM33256_SPI_CS);

	unlock();
}

void fm33256b_read_data(uint16_t addr, uint8_t *data, uint16_t len) {
	lock();

	spi_selectChip(FM33256_SPI, FM33256_SPI_CS);
	uint8_t cmd[3] = {FM33_READ, addr >> 8, addr & 0xff};
	for(int i = 0; i < 3; i++)
		spi_write(FM33256_SPI, cmd[i]);

	for(int i = 0; i < len; i++) {
		uint16_t read;
		spi_write(FM33256_SPI, 0xff);
		spi_read(FM33256_SPI, &read);
		data[i] = (uint8_t) read;
	}
	spi_unselectChip(FM33256_SPI, FM33256_SPI_CS);

	unlock();
}

void fm33256b_lock_upper_quarter(void) {
	lock();

	uint8_t cmd[] = {FM33_WRSR, 0x04};
	fm33256b_write(cmd, 2);

	unlock();
}

void fm33256b_unlock_upper_quarter(void) {
	lock();

	uint8_t cmd[] = {FM33_WRSR, 0x00};
	fm33256b_write(cmd, 2);

	unlock();
}

int fm33256b_clock_halt(void) {
	lock();

	uint8_t cmd[3];

	cmd[0] = FM33_RDPC;
	cmd[1] = FM33_RTC_ALARM_CTRL;
	fm33256b_read(&cmd[0], 2, &cmd[2], 1);	
	cmd[0] = FM33_WRPC;;
	cmd[2] |= FM33_MASK_OSCEN;

	fm33256b_write(cmd, 3);

	unlock();
	return 0;
}

int fm33256b_clock_resume(void) {
	lock();

	uint8_t cmd[3];

	cmd[0] = FM33_RDPC;
	cmd[1] = FM33_RTC_ALARM_CTRL;
	fm33256b_read(&cmd[0], 2, &cmd[2], 1);	
	cmd[0] = FM33_WRPC;
	cmd[2] &= ~FM33_MASK_OSCEN;

	fm33256b_write(cmd, 3);

	unlock();
	return 0;
}

int fm33256b_clock_write_burst(struct fm33256b_clock *clock) {

	uint8_t cmd[3];
	uint8_t data[sizeof(*clock)+2];
	if (!clock)
		return -1;
	
	lock();

	// Copy clk to read
	cmd[0] = FM33_RDPC;
	cmd[1] = FM33_RTC_ALARM_CTRL;
	fm33256b_read(cmd, 2, &cmd[2], 1);
	cmd[0] = FM33_WRPC;
	cmd[2] |= FM33_MASK_CLKW;
	fm33256b_write(cmd, 3);

	clock->seconds 	= sane_to_bcd(clock->seconds & 0x7f);
	clock->minutes	= sane_to_bcd(clock->minutes);
	clock->hour 	= sane_to_bcd(clock->hour);
	clock->day 	= sane_to_bcd(clock->day);
	clock->date 	= sane_to_bcd(clock->date);
	clock->month 	= sane_to_bcd(clock->month);
	clock->year 	= sane_to_bcd(clock->year);

	data[0] = FM33_WRPC;
	data[1] = FM33_SECONDS;
	memcpy(&data[2], (uint8_t *)clock, sizeof(*clock));

	fm33256b_write(data, sizeof(data));

	cmd[2] &= ~FM33_MASK_CLKW;
	fm33256b_write(cmd, 3);

	unlock();
	return 0;
}

int fm33256b_clock_read_burst(struct fm33256b_clock *clock) {
	uint8_t cmd[3];
	if (!clock)
		return -1;
	
	lock();

	//  Set read
	cmd[0] = FM33_RDPC;
	cmd[1] = FM33_RTC_ALARM_CTRL;
	fm33256b_read(cmd, 2, &cmd[2], 1);
	cmd[0] = FM33_WRPC;
	cmd[2] |= FM33_MASK_CLKR;
	fm33256b_write(cmd, 3);
	
	cmd[2] &= ~FM33_MASK_CLKR;
	fm33256b_write(cmd, 3);

	// Read clk
	cmd[0] = FM33_RDPC;	
	cmd[1] = FM33_SECONDS;
	fm33256b_read(cmd, 2, (uint8_t *) clock, sizeof(*clock));

	unlock();
	
	clock->seconds 	= bcd_to_sane(clock->seconds & 0x7f);
	clock->minutes 	= bcd_to_sane(clock->minutes);
	clock->hour 	= bcd_to_sane(clock->hour);
	clock->day 	= bcd_to_sane(clock->day);
	clock->date 	= bcd_to_sane(clock->date);
	clock->month 	= bcd_to_sane(clock->month);
	clock->year 	= bcd_to_sane(clock->year);

	return 0;
}
/*
*/
int fm33256b_init(void) {

	/* Spi options */
	spi_options_t spi_opt = {};
	spi_opt.modfdis = 1;

	/* Init CS 2 (TEMP1) */
	spi_opt.baudrate = 8000000;
	spi_opt.bits = 8;
	spi_opt.reg = FM33256_SPI_CS;
	spi_opt.spi_mode = 0;
	spi_opt.stay_act = 1;
	spi_opt.trans_delay = 5;
	int result = spi_setupChipReg(FM33256_SPI, &spi_opt, sysclk_get_peripheral_bus_hz(FM33256_SPI));
	if (result != SPI_OK) {
		log_error("Could not setup SPI");
		return -1;
	}

	fm33256b_lock = xSemaphoreCreateMutex();
	if (fm33256b_lock == NULL) {
		log_error("No semphr");
		return -1;
	}

	/* Setup square wave output */
#if 0 //! This have been removed, since we cannot call spi_write in _init function, due to SPI locking
	uint8_t cmd[3];
	cmd[0] = FM33_WRPC;
	cmd[1] = FM33_COMPANION_CTRL;
	cmd[2] = 0x38;
	fm33256b_write_ena();
	fm33256b_write(cmd, 3);
#endif

	return 0;
}

int fm33256b_time_to_clock(time_t *time, struct fm33256b_clock *clock) {
	struct tm *t;

	t = gmtime(time);
	if (t && clock) {
		clock->seconds 	= t->tm_sec;
		clock->minutes 	= t->tm_min;
		clock->hour 	= t->tm_hour;
		clock->date 	= t->tm_mday;
		clock->month 	= t->tm_mon + 1;
		clock->day 	= t->tm_wday;
		clock->year 	= t->tm_year-100;
		return 0;
	} else {
		return -1;
	}
}

int fm33256b_clock_to_time(time_t *time, struct fm33256b_clock *clock) {
	struct tm t;

	if (!time || !clock)
		return -1;

	t.tm_sec = clock->seconds;
	t.tm_min = clock->minutes;
	t.tm_hour = clock->hour;
	t.tm_mday = clock->date;
	t.tm_mon = clock->month - 1;
	t.tm_year = clock->year+100;
	t.tm_isdst = 0;

	*time = mktime(&t);

	return 0;
}


void rtc_get_time(timestamp_t * time) {

	/* Get time from RTC */
	struct fm33256b_clock clock;
	fm33256b_clock_read_burst(&clock);

	/* Convert to timestamp */
	fm33256b_clock_to_time((time_t *) &time->tv_sec, &clock);
	time->tv_nsec = 0;

}

void rtc_set_time(timestamp_t * time) {

	/* Convert to clock */
	struct fm33256b_clock clock;
	fm33256b_time_to_clock((time_t *) &time->tv_sec, &clock);

	/* Set time to RTC */
	fm33256b_clock_write_burst(&clock);

}
