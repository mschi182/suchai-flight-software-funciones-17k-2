#include <stdio.h>
#include <stdint.h>
#include <math.h>

#include <FreeRTOS.h>
#include <task.h>

#include <util/error.h>
#include <util/driver_debug.h>

#include <dev/i2c.h>

#include "mpu3300.h"

/**
 * @file mpu3300.c
 * Commands for mpu3300 gyro
 *
 * @author Morten Jensen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

int mpu3300_full_scale_reading = 0;

int mpu3300_read_raw(mpu3300_gyro_raw_t * gyro_reading) {
	uint8_t cmd[1];
	uint8_t res[6];
	int result;

	cmd[0] =  MPUREG_GYRO_XOUT_H;
	result = i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 1, res, 6, 20);
	gyro_reading->gyro_x = (int16_t) ((res[0] << 8) + res[1]);
	gyro_reading->gyro_y = (int16_t) ((res[2] << 8) + res[3]);
	gyro_reading->gyro_z = (int16_t) ((res[4] << 8) + res[5]);
	if (result == E_NO_ERR)
		return 0;
	return -1;
}

int mpu3300_selftest(void) {
	uint8_t cmd[2];
	uint8_t res[6];
	int i;

	uint8_t factory_trim[3];
	
	mpu3300_gyro_raw_t gyro_with_self_test;
	mpu3300_gyro_raw_t gyro_no_self_test;

	int32_t gyro_self_test_res[3];

	float ft_x;
	float ft_y;
	float ft_z;

	float tmp;


	/* First read register to get current values */
	cmd[0] = MPUREG_GYRO_CONFIG;
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 1, res, 1, 1000) != E_NO_ERR)
		return -1;

	/* Set the selftest bits */
	cmd[1] = res[0] | 0b11100000;
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 1000) != E_NO_ERR)
		return -1;
	/* Get factory trim values */
	cmd[0] =  MPUREG_SELF_TEST_X;
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 1, factory_trim, 3, 1000) != E_NO_ERR)
		return -1;

	/* Wait for data and then read gyro */
	vTaskDelay(50 / portTICK_RATE_MS);

	if (mpu3300_read_raw(&gyro_with_self_test) != 0)
		return -1;

	/* Now read gyro without selftest */

	/* Clear the selftest bits */
	cmd[0] = MPUREG_GYRO_CONFIG;
	cmd[1] = res[0] & 0b00011111;
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 1000) != E_NO_ERR)
		return -1;

	/* Wait for data and then read gyro */
	vTaskDelay(50 / portTICK_RATE_MS);
	if (mpu3300_read_raw(&gyro_no_self_test) != 0)
		return -1;

	for (i = 0; i < 3; i++) {
		factory_trim[i] = factory_trim[i] & 0x1f;
	}

	/* Selftest calculations */
	if (factory_trim[0] != 0) {
		ft_x = 25 * 145.6 * powf(1.046, (float) factory_trim[0] - 1);
	} else {
		ft_x = 0.0;
	}
	
	if (factory_trim[1] != 0) {
		ft_y = -25 * 145.6 * powf(1.046, (float) factory_trim[1] - 1);
	} else {
		ft_y = 0.0;
	}
	
	if (factory_trim[1] != 0) {
		ft_z = 25 * 145.6 * powf(1.046, (float) factory_trim[2] - 1);
	} else {
		ft_z = 0.0;
	}

	gyro_self_test_res[0] = gyro_with_self_test.gyro_x - gyro_no_self_test.gyro_x;
	gyro_self_test_res[1] = gyro_with_self_test.gyro_y - gyro_no_self_test.gyro_y;
	gyro_self_test_res[2] = gyro_with_self_test.gyro_z - gyro_no_self_test.gyro_z;

	tmp = (gyro_self_test_res[0] - ft_x) / ft_x;
	if (tmp > 0.14 || tmp < -0.14) {
		driver_debug(DEBUG_MPU3300, "Gyro_x faild selftest: %f\r\n", tmp);
		return -2;
	}

	tmp = (gyro_self_test_res[1] - ft_y) / ft_y;
	if (tmp > 0.14 || tmp < -0.14) {
		driver_debug(DEBUG_MPU3300, "Gyro_y faild selftest: %f\r\n", tmp);
		return -2;
	}

	tmp = (gyro_self_test_res[2] - ft_z) / ft_z;
	if (tmp > 0.14 || tmp < -0.14) {
		driver_debug(DEBUG_MPU3300, "Gyro_z faild selftest: %f\r\n", tmp);
		return -2;
	}

	return 0;
}

int mpu3300_init(mpu3300_bandwidth_t bandwidth, mpu3300_full_scale_reading_t full_scale) {
	uint8_t cmd[2];

	/* Set full scale range */
	cmd[0] = MPUREG_GYRO_CONFIG;
	if (full_scale == MPU3300_FSR_225) {
		cmd[1] = 0;
	} else {
		cmd[1] = 1 << 3;
	}
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 100) != E_NO_ERR)
		return -1;

	mpu3300_full_scale_reading = full_scale;

	/* set bandwidth */
	cmd[0] = MPUREG_CONFIG;
	cmd[1] = bandwidth;
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 100) != E_NO_ERR)
		return -1;

	/* Set clock source to be X axis gyro pll. The gyro can not be in low power 
	 * mode when using pll so switch to internal clock before low power mode */
	cmd[0] = MPUREG_PWR_MGMT_1;
	cmd[1] = 1;
	if (i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 100) != E_NO_ERR)
		return -1;

	return 0;
}

int mpu3300_reset(void) {
	uint8_t cmd[2];
	int result;

	cmd[0] =  MPUREG_PWR_MGMT_1;
	cmd[1] = 1 << 7;
	result = i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 100);

	if (result == E_NO_ERR)
		return 0;
	return -1;
}


int mpu3300_sleep(uint8_t sleep) {
	uint8_t cmd[2];
	uint8_t res[1];
	int result;



	cmd[0] =  MPUREG_PWR_MGMT_1;
	result = i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 1, res, 1, 100);

	if (result != E_NO_ERR)
		return -1;

	cmd[1] = res[0];

	if (sleep == 0)
		cmd[1] &= ~(1 << 6);
	if (sleep == 1)
		cmd[1] |= (1 << 6);

	result = i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 2, NULL, 0, 100);
	if (result != E_NO_ERR)
		return -1;


	return 0;
}

int mpu3300_read_gyro(mpu3300_gyro_t * gyro_readings) {
	float scale;	// LSB/degrees/s
	mpu3300_gyro_raw_t gyro;

	if (mpu3300_full_scale_reading == MPU3300_FSR_225) {
		scale = 145.6;
	} else {
		scale = 72.8;
	}

	if (mpu3300_read_raw(&gyro) != 0)
		return -1;
	gyro_readings->gyro_x = gyro.gyro_x / scale;
	gyro_readings->gyro_y = gyro.gyro_y / scale;
	gyro_readings->gyro_z = gyro.gyro_z / scale;

	return 0;
}

int mpu3300_read_temp(float * temp) {
	uint8_t cmd[1];
	uint8_t res[2];
	int result;

	cmd[0] =  MPUREG_TEMP_OUT_H;
	result = i2c_master_transaction(2, MPU3300_I2C_ADDR, cmd, 1, res, 2, 20);
	
	*temp = ((int16_t) ((res[0] << 8) + res[1])) / 340. + 36.53;

	if (result == E_NO_ERR)
		return 0;
	return -1;
}
