.. A3200 SDK Lite Main Documentation

.. |A3200SDK| replace:: A3200 SDK Lite

************
Introduction
************

This manual describes the NanoMind |A3200SDK| software package.
It contains the main software modules for a basic |A3200SDK| application.
Three GomSpace libraries are included to support boot and development of the A3200 application.
These three libraries are:

* ASF library
* A3200 library
* Util library

The following block diagram shows the architecture for the NanoMind |A3200SDK| software package:

.. figure:: img/a3200_sdk_lite.png
   :width: 80 %
   
   |A3200SDK| Architecture

The main software modules and the three libraries are described in the following chapters.

************************************
GomSpace |A3200SDK| Software modules
************************************

This chapter describes the main software modules for the |A3200SDK| application. 
It contains the following modules:

* main.c
* task_init.c
* task_blink.c
* task_wdt_clear.c
* task_sample_sensors.c

main.c
######

The main.c module contains the calls to required initialisation and startup 
functions for the A3200 board.
Before starting any tasks, the Watchdog Timer (wdt) is initialised and cleared to
prevent wdt reset during initialisation.

The included tasks |A3200SDK| application are initialised by the task ``init_task()`` 
in the module ``task_init.c``.
The task ``init_task()`` is started from ``main()`` by calling ``vTaskStartScheduler()``.
After that ``main.c`` simply sleeps forever, leaving the rest to the individual tasks.

Listing of ``main.c``:

.. literalinclude:: ../src/main.c
   :language: c
   :lines: 27-
   :linenos:

The first lines in ``main()`` disables the wdt, clears it, initialises it with a 
timeout of 5 seconds and finally enables it:

.. code-block:: c
   :linenos:

   /* Reset watchdog */
   wdt_disable();
   wdt_clear();
   wdt_opt_t wdtopt = { .us_timeout_period = 5000000 };
   wdt_enable(&wdtopt);

task_init.c
###########

The ``task_init.c`` module contains the initialisation of all the tasks included in the |A3200SDK| application.
The following tasks are created by calling ``xTaskCreate()`` for each task:

* ``task_blink``
* ``task_wdt_clear``
* ``task_sample_sensors``

Before the individual tasks are created, the ``init_task()`` task initialiases the different 
interfaces on the AVR32 processor and the peripheral devices and sensors on the A3200 board.
Please refer to :doc:`/lib/liba3200/doc/liba3200` for further details.

See the NanoMind A3200 Datasheet for further details on the AVR32 processor and the peripheral devices and sensors
(available on the GomSpace website http://www.gomspace.com).

The drivers and the API functions for peripheral devices and sensors on the A3200 board are located 
in the GomSpace A3200 library. Please refer to :doc:`/lib/liba3200/doc/liba3200` for further details 
on the GomSpace A3200 library.

task_blink.c
############

Code listing:

.. literalinclude:: ../src/task_blink.c
   :language: c
   :lines: 11-
   :emphasize-lines: 3,6-7
   :linenos:

task_wdt_clear.c
################

Code listing:

.. literalinclude:: ../src/task_wdt_clear.c
   :language: c
   :lines: 6-
   :linenos:


task_sample_sensors.c
#####################

Code listing:

.. literalinclude:: ../src/task_sample_sensors.c
   :language: c
   :lines: 36-
   :linenos:
