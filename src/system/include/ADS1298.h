/*
 * ADS1298.h
 * Header libreria ADS1298
 * Created: 25-03-2018 2:35:16
 *  Author: Diego Benavente
 */ 

#ifndef ADS1298_H_
#define ADS1298_H_

#include <asf.h>
#include <stdint.h>
#include "perifericos.h"

void adcSelect(void);
void adcDeselect(void);
void adcWrite(uint8_t data);
void adcRead(uint8_t * buf, size_t size);
void adcRESET(void);
void adcSDATAC(void);
void adcWREG(uint8_t reg, uint8_t value);
void adcRDATAC(void);
void adcSTART(void);
uint8_t adcRREG(uint8_t reg);
uint8_t adcConfig(void);

//Comandos
#define ADS1298_OP_RESET				0x06
#define ADS1298_OP_SDATAC				0x11
#define ADS1298_OP_WREG					0x40
#define ADS1298_OP_WREG_REG_MASK		0x1F
#define ADS1298_OP_RDATAC				0x10
#define ADS1298_OP_START				0x08
#define ADS1298_OP_RREG					0x20
#define ADS1298_OP_RREG_REG_MASK		0x1F

//Registros
#define ADS1298_ID						0x00
#define ADS1298_CONFIG1					0x01
#define ADS1298_CONFIG2					0x02
#define ADS1298_CONFIG3					0x03
#define ADS1298_LOFF					0x04
#define ADS1298_CH1SET					0x05
#define ADS1298_CH2SET					0x06
#define ADS1298_CH3SET					0x07
#define ADS1298_CH4SET					0x08
#define ADS1298_CH5SET					0x09
#define ADS1298_CH6SET					0x0A
#define ADS1298_CH7SET					0x0B
#define ADS1298_CH8SET					0x0C
#define ADS1298_CHNSET_PD				0x80
#define ADS1298_CHNSET_GAIN_6			0x00
#define ADS1298_CHNSET_GAIN_1			0x10
#define ADS1298_CHNSET_GAIN_2			0x20
#define ADS1298_CHNSET_GAIN_3			0x30
#define ADS1298_CHNSET_GAIN_4			0x40
#define ADS1298_CHNSET_GAIN_8			0x50
#define ADS1298_CHNSET_GAIN_12			0x60
#define ADS1298_CHNSET_MUX_NORMAL		0x00
#define ADS1298_CHNSET_MUX_SHORTED		0x01
#define ADS1298_CHNSET_MUX_RLD_MEAS		0x02
#define ADS1298_CHNSET_MUX_MVDD			0x03
#define ADS1298_CHNSET_MUX_TEMP			0x04
#define ADS1298_CHNSET_MUX_TEST			0x05
#define ADS1298_CHNSET_MUX_RLD_DRP		0x06
#define ADS1298_CHNSET_MUX_RLD_DRN		0x07

#endif /* ADS1298_H_ */