/**
 * Task timesync and csp time sync function
 *
 * @author Kristian Bay
 * Copyright 2015 GomSpace ApS. All rights reserved.
 */

#ifndef TASK_TIMESYNC_H_
#define TASK_TIMESYNC_H_

int do_csp_timesync(uint8_t tsync_node, uint32_t timeout);
void task_timesync(void * pvParameters);

#endif /* TASK_TIMESYNC_H_ */
