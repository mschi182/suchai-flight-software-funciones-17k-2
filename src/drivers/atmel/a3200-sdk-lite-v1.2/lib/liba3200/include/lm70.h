/**
 * NanoCom firmware
 *
 * @author Johan De Claville Christiansen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#ifndef LM70_H_
#define LM70_H_

/**
 * Startup LM70 SPI driver
 */
void lm70_init(void);

/**
 * @param sensor id, either 1 or 2
 * @return 16-bit temp value from sensor as float
 */
int16_t lm70_read_temp(int sensor);

#endif /* LM70_H_ */
