/**
 * NanoMind firmware
 *
 * @author Johan De Claville Christiansena
 * @author Morten Jensen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <conf_a3200.h>

#include <sysclk.h>
#include <wdt.h>
#include <gpio.h>
#include <sdramc.h>
#include <reset_cause.h>

#include <dev/usart.h>
#include <dev/i2c.h>
#include <dev/cpu.h>

#include <FreeRTOS.h>
#include <task.h>

#include <led.h>
#include <pwr_switch.h>

int main() {

	/* Reset watchdog */
	wdt_disable();
	wdt_clear();
	wdt_opt_t wdtopt = { .us_timeout_period = 5000000 };
	wdt_enable(&wdtopt);

	/* Initialize interrupt handling.
	 * This function call is needed, when using libasf startup.S file */
	INTC_init_interrupts();

	/* Init CLK */
	sysclk_init();

	/* Init pwr channels */
	pwr_switch_init();

	/* At this point we only power the interstages. 
	 * MLX and finesunsensors are powered on in init task */ 
	pwr_switch_disable(PWR_GSSB);
	pwr_switch_disable(PWR_GSSB2);

	/* Init SDRAM, do this before malloc is called */
	sdramc_init(sysclk_get_cpu_hz());

	/* We have to trust that the SDRAM is working by now */
#if 0
	extern void *heap_start, *heap_end;
	heap_start = (void *) 0xD0000000 + 0x100000;
	heap_end = (void *) 0xD0000000 + 0x2000000 - 1000;
#endif

	/* Init USART for debugging */
	static const gpio_map_t USART_GPIO_MAP = {
		{USART_RXD_PIN, USART_RXD_FUNCTION},
		{USART_TXD_PIN, USART_TXD_FUNCTION},
	};
	gpio_enable_module(USART_GPIO_MAP,
			sizeof(USART_GPIO_MAP) / sizeof(USART_GPIO_MAP[0]));
	usart_init(USART_CONSOLE, sysclk_get_peripheral_bus_hz(USART), 500000);
	usart_stdio_id = USART_CONSOLE;

	/* Init LED */
	led_init();
	led_on(LED_CPUOK);

	/* Start init task at highest priority */
	void init_task(void * param);
	xTaskCreate(init_task, "INIT", 4000, NULL, configMAX_PRIORITIES - 1, NULL);

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory
	 * to create the idle task. */
	while(1) cpu_reset();

}
