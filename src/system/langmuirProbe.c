//
// Created by kaminari on 10-07-18.
//
#include "langmuirProbe.h"
static const char* tag = "langmuirProbe";

uint16_t j;
uint16_t arreglo [1299]; //2000 es el ideal
//int* puntero = arreglo;
//int32_t diff;
const uint8_t ANALOG_COUNT = 6;
// unsigned long mil = 1000;
void sysclk_init (void); // antes: void sysclk_init (void);
// Buffer to send data to SPI slave
uint16_t txdata;
//Buffer to receive data from SPI slave
uint16_t rxdata;
clock_t seconds;
//const uint8_t TIMER = 400; // [ms]
/*
void spi_init_module(void){
    spi_options_t my_spi_options={
            // The SPI channel to set up : Memory is connected to CS1
            DATA_OUT_SPI_NPCS,
            // Preferred baudrate for the SPI.
            DATA_OUT_SPI_BAUDRATE,// 1.000.000: valor original
            // Number of bits in each character (8 to 16).
            8,
            // Delay before first clock pulse after selecting slave (in PBA clock periods).
            0,
            // Delay between each transfer/character (in PBA clock periods).
            0,
            // Sets this chip to stay active after last transfer to it.
            1,
            // Which SPI mode to use when transmitting.
            SPI_MODE_2,
            // Disables the mode fault detection.
            // With this bit cleared, the SPI master mode will disable itself if another
            // master tries to address it.
            1
    };
    static const gpio_map_t DATA_OUT_SPI_GPIO_MAP = {
            {DATA_OUT_SPI_SCK_PIN,  DATA_OUT_SPI_SCK_FUNCTION },
            {DATA_OUT_SPI_MISO_PIN, DATA_OUT_SPI_MISO_FUNCTION},
            {DATA_OUT_SPI_MOSI_PIN, DATA_OUT_SPI_MOSI_FUNCTION},
            {DATA_OUT_SPI_NPCS0_PIN, DATA_OUT_SPI_NPCS0_FUNCTION},
    };
    gpio_enable_module(DATA_OUT_SPI_GPIO_MAP, sizeof(DATA_OUT_SPI_GPIO_MAP) / sizeof(DATA_OUT_SPI_GPIO_MAP[0]));
    //Init SPI module as master
    spi_initMaster(DATA_OUT_SPI,&my_spi_options);
    //Setup configuration for chip connected to CS1
    spi_setupChipReg(DATA_OUT_SPI,&my_spi_options,sysclk_get_pba_hz());
    //Allow the module to transfer data
    spi_enable(DATA_OUT_SPI);
}
*/
//ISR(nombre_de_la_interrupcion,AVR32_448_IRQ_GROUP,0){//adcIsr, ADC_DREADY_IRQ, ADC_DREADY_INTLVL){

    /* lo del magnetometro
    if(gpio_get_pin_interrupt_flag(ADC_DREADY)){
        adcSelect();
        adcRead((uint8_t *) buf, sizeof(buf)/sizeof(buf[0]));
        adcDeselect();
        dready = 1;
        gpio_clear_pin_interrupt_flag(ADC_DREADY);
    }*/

//}
void resetADC(void){
    set_pin_ADC_convst_HIGH;
    set_pin_ADC_reset_HIGH;
    delay_ms(1);
    set_pin_ADC_reset_LOW;
    delay_ms(1);
    set_pin_ADC_convst_LOW;
}
void writeHeader(void){
    printf("\nCada lectura (de 16 bits) se entrega como 2 bytes separados por '||'.\n");
    printf("Sampling frequency = 83,3 [Hz].\n");
    printf("SAMPLE, HIGH_GAIN, LOW_GAIN, TEMP1, TEMP2, V5, V6 (no se utiliza).\n");
}
void una_lectura_del_adc_del_LP(void) {
    conf_pin_ADC_CS;         // Output
    conf_pin_ADC_reset;
    conf_pin_ADC_convst;
    conf_pin_ADC_BUSY;       // Input

    set_pin_ADC_CS_HIGH; //<-----prueba
    set_pin_ADC_reset_LOW; //<-----prueba
    set_pin_ADC_convst_HIGH; //<-----prueba

    spi_init(); //originalmente spi_init(), funcion que inicializa la comunicacion SPI

    writeHeader();
    resetADC();

    // enable timer overflow interrupt for both Timer0 and Timer1
    //TIMSK=(1<<TOIE0) | (1<<TOIE1);

    //irq_initialize_vectors();
    //irq_register_handler(nombre_de_la_interrupcion,AVR32_448_IRQ,0);
    //INTC_register_interrupt(&nombre_de_la_interrupcion,,);//, ADC_DREADY_IRQ, ADC_DREADY_INTLVL);
    //cpu_irq_enable();

}
void startConversion(void){
    set_pin_ADC_convst_LOW;
    set_pin_ADC_convst_HIGH;
    while(read_pin_ADC_BUSY==1) { //#####################################
    }
}
void startAcquisition(void){
    set_pin_ADC_convst_HIGH;
    set_pin_ADC_convst_LOW;
}
void readADC(unsigned char show){
    uint16_t highbyte;
    uint16_t lowbyte;
    uint8_t txdata = 0x00;
    spi_selectChip(DATA_OUT_SPI, DATA_OUT_SPI_NPCS);
    while (!spi_is_tx_empty(DATA_OUT_SPI)) {}
    spi_put(DATA_OUT_SPI, txdata);
    while (!spi_is_tx_empty(DATA_OUT_SPI)) {}
    highbyte = spi_get(DATA_OUT_SPI);
    spi_put(DATA_OUT_SPI, txdata);
    while (!spi_is_tx_empty(DATA_OUT_SPI)) {}
    lowbyte = spi_get(DATA_OUT_SPI);
    spi_unselectChip(DATA_OUT_SPI, DATA_OUT_SPI_NPCS);
    if (show == 4) { //if (show == 1) {
        arreglo[j] = ((uint16_t)(0x00FF & highbyte) << 8) | (0x00FF & lowbyte);
        //printf("%u = ", j);//~~~~~~~~~~~~~~~~~~~~~
        //printf("%u\n", arreglo[j]);//~~~~~~~~~~~~~~~~~~~~~
    }
}
void logData(void){
    startConversion();
    for (uint8_t i = 0; i < ANALOG_COUNT; i++) {
        readADC(i);//readADC(1);
    }
}
void una_lectura_del_adc_del_LP_2(void) {
    j = 0;
    //FATFS fs;
    while(j < 1299){ //max. que aguanta 1299. originalmente quiero 2000 mediciones
        logData();
        j++;
    }
    //sd_mmc_spi_write_word(arreglo[1]);
    //sd_mmc_spi_start_write_blocks(*puntero, arreglo);
    // /*
    j = 0;
    //printf("j3 = %u \n", j);
    printf("BAUDRATE = %u\n", DATA_OUT_SPI_BAUDRATE);

    for (j = 0; j < 1299; j++) {
        seconds = clock();// /CLOCKS_PER_SEC; para tener los segundos
        //sd_mmc_spi_write_word(arreglo[j]);

        //printf("tiempo=%ld", seconds);
        printf("%u = ", arreglo[j]);
        printf("arreglo[%u]\n", j);
    }
    // */
}