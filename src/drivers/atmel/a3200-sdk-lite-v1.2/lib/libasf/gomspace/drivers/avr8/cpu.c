/*
 * cpu.c
 *
 *  Created on: Sep 8, 2009
 *      Author: johan
 */

#include <FreeRTOS.h>
#include <avr/wdt.h>
#include <avr/interrupt.h>
#include <dev/cpu.h>

void cpu_reset(void) {
	wdt_enable(WDTO_15MS);
	cli();
	volatile int i;
	while(1) {
		i++;
	}
}

// Function Pototype
void cpu_init(void) __attribute__((naked, used)) __attribute__((section(".init3")));

// Function Implementation
void cpu_init(void) {
    wdt_disable();
}
