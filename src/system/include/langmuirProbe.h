//
// Created by kaminari on 10-07-18.
//

#ifndef _SUCHAI_FLIGHT_SOFTWARE_LANGMUIR_H_
#define _SUCHAI_FLIGHT_SOFTWARE_LANGMUIR_H_

#include "config.h"
#include "repoCommand.h"
#include "init.h"
// #include "main.h"
// #include "asf.h"
#include "perifericos.h"
#include "ADS1298.h"
#include <spi.h>
#include <time.h>
#include <stdio.h>
//#include "conf_spi_master.h"

#include "sd_mmc_spi.h"
//#include <string.h>   //SD diego
//#include "setup.h"    //SD diego
//#include <ff.h>       //SD diego
//#include <integer.h>  //SD diego


//==========================================================================================================¿¿¿
#define conf_pin_ADC_CS          gpio_enable_gpio_pin(AVR32_PIN_PA07)
#define conf_pin_ADC_reset       gpio_enable_gpio_pin(AVR32_PIN_PA31)
#define conf_pin_ADC_convst      gpio_enable_gpio_pin(AVR32_PIN_PA29)

// **configure as input**
#define conf_pin_ADC_BUSY        gpio_enable_gpio_pin(AVR32_PIN_PA30)

// **put as HIGH**
#define set_pin_ADC_CS_HIGH      gpio_set_gpio_pin(AVR32_PIN_PA07)
#define set_pin_ADC_reset_HIGH   gpio_set_gpio_pin(AVR32_PIN_PA31)
#define set_pin_ADC_convst_HIGH  gpio_set_gpio_pin(AVR32_PIN_PA29)

// **put as LOW**
#define set_pin_ADC_CS_LOW       gpio_clr_gpio_pin(AVR32_PIN_PA07)
#define set_pin_ADC_reset_LOW    gpio_clr_gpio_pin(AVR32_PIN_PA31)
#define set_pin_ADC_convst_LOW   gpio_clr_gpio_pin(AVR32_PIN_PA29)

// **read pin**
#define read_pin_ADC_BUSY        gpio_get_pin_value(AVR32_PIN_PA30)

/**
 * Register on board computer related (OBC) commands
 */
/*void cmd_subsys_init(void);

int get_gps_data(char *fmt, char *params, int nparams);
int get_dpl_data(char *fmt, char *params, int nparams);
int get_prs_data(char *fmt, char *params, int nparams);
int open_dpl_la(char *fmt, char *params, int nparams);
int close_dpl_la(char *fmt, char *params, int nparams);
int open_dpl_sm(char *fmt, char *params, int nparams);
int close_dpl_sm(char *fmt, char *params, int nparams);
int send_iridium_data(char *fmt, char *params, int nparams);
int send_iridium_msg1(char *fmt, char *params, int nparams);
int send_iridium_msg2(char *fmt, char *params, int nparams);*/
// void spi_init_master (void);
// void spi_init_slave (void);

void spi_init_module(void);
void resetADC(void);
void startConversion(void);
void startAcquisition(void);
void readADC(unsigned char show);
void writeHeader(void);
void logData(void);
unsigned char spi_tranceiver (unsigned char data);
void una_lectura_del_adc_del_LP(void);
void una_lectura_del_adc_del_LP_2(void);

#endif //_SUCHAI_FLIGHT_SOFTWARE_LANGMUIR_H_