#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <inttypes.h>
#include <string.h>
#include <time.h>

#include <util/console.h>
#include <util/hexdump.h>
#include "spn_fl512s.h"

int cmd_spn_fl512s_read_sr(struct command_context *ctx) {
	uint8_t sr = spn_fl512s_read_status_register();
	printf("Status register: %"PRIx8"\r\n", sr);
	return CMD_ERROR_NONE;
}

int cmd_spn_fl512s_read_id(struct command_context *ctx) {
	char data[64];
	spn_fl512s_read_device_id(data);
	hex_dump(data, 64);
	return CMD_ERROR_NONE;
}


int cmd_spn_fl512s_write(struct command_context *ctx) {

	uint16_t len;
	uint32_t addr;
	char data[256];

	if (ctx->argc != 3)
		return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);
	sscanf(ctx->argv[2], "%[^\t\n]", data);
	len = strlen(data);
	printf("%lx\r\n", addr);
	spn_fl512s_write_data(addr, (uint8_t *) data, len);

	return CMD_ERROR_NONE;
}

int cmd_spn_fl512s_read(struct command_context *ctx) {
	uint8_t data[256];
	uint32_t addr;

	if (ctx->argc != 2)
		return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);
	printf("%lx\r\n", addr);
	spn_fl512s_read_data(addr, data, 256);

	hex_dump(data, 256);

	return CMD_ERROR_NONE;
}

int cmd_spn_fl512s_erase(struct command_context *ctx) {
	uint32_t addr;

	if (ctx->argc != 2)
		return CMD_ERROR_SYNTAX;

	sscanf(ctx->argv[1], "%lx", &addr);
	printf("%lx\r\n", addr);
	spn_fl512s_erase_block(addr);

	return CMD_ERROR_NONE;
}

int cmd_spn_fl512s_erase_chip(struct command_context *ctx) {
	spn_fl512s_erase_chip();
	return CMD_ERROR_NONE;
}

command_t __sub_command spn_fl512s_subcommands[] = {
	{
		.name = "readsr",
		.help = "Read Status Register",
		.handler = cmd_spn_fl512s_read_sr,
	},{
		.name = "readid",
		.help = "Read CFI/ID",
		.handler = cmd_spn_fl512s_read_id,
	},{
		.name = "read",
		.help = "Read Data",
		.usage = "<addr [hex]>",
		.handler = cmd_spn_fl512s_read,
	},{
		.name = "write",
		.help = "Write Data",
		.usage = "<addr [hex]> <data> (max len 256)",
		.handler = cmd_spn_fl512s_write,
	},{
		.name = "erase",
		.help = "Erase Data (block size 256kB)",
		.usage = "<addr [hex]>",
		.handler = cmd_spn_fl512s_erase,
	},{
		.name = "erase_chip",
		.help = "Erase all data",
		.handler = cmd_spn_fl512s_erase_chip,
	},
};

command_t __sub_command __root_command spn_fl512s_commands[] = {
	{
		.name = "spn",
		.help = "spn_fl512s commands",
		.chain = INIT_CHAIN(spn_fl512s_subcommands),
	},
};

void cmd_spn_fl512s_setup(void) {
	command_register(spn_fl512s_commands);
}
