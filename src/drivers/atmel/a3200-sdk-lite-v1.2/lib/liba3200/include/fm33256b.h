/**
 * @file fm33256b.h
 * Bit banging serial driver for the Maxim FM33256B RTC
 *
 * @note Data is transmitted LSB first. Write data is
 * sampled on the rising edge of SCLK. Read data is
 * output on the falling edge of SCLK.
 *
 * @author Jeppe Ledet-Pedersen
 * @author Morten Jensen	
 * Copyright 2013 GomSpace ApS. All rights reserved.
 */

#ifndef _FM33256B_H_
#define _FM33256B_H_

#include <unistd.h>

/* Chip spi controller addr */
#define FM33_SPI_ADDR 0

/* Op-code Commands */
#define FM33_WREN 	0x06	// Set Write Enable Latch
#define FM33_WRDI	0x04	// Write Disable
#define FM33_RDSR	0x05	// Read Status Register
#define FM33_WRSR	0x01	// Write Status Register
#define FM33_READ	0x03	// Read Memory Data
#define FM33_WRITE	0x02	// Write Memory Data
#define FM33_RDPC	0x13	// Read Proc. Companion
#define FM33_WRPC	0x12	// Write Proc. Companion

/* Register map */
#define FM33_ALARM_MONTH	0x1D	// Alarm Month [01-12]
#define FM33_ALARM_DATE		0x1C	// Alarm Date [01-31]
#define FM33_ALARM_HOURS	0x1B	// Alarm Hours [00-23]
#define FM33_ALARM_MINUTES	0x1A	// Alarm Minutes [00-59]
#define FM33_ALARM_SECONDS	0x19	// Alarm Seconds [00-59]
#define FM33_COMPANION_CTRL	0x18	// Companion Control
#define FM33_SERIAL_NR_7	0x17	// Serial Number 7
#define FM33_SERIAL_NR_6	0x16	// Serial Number 6
#define FM33_SERIAL_NR_5	0x15	// Serial Number 5
#define FM33_SERIAL_NR_4	0x14	// Serial Number 4
#define FM33_SERIAL_NR_3	0x13	// Serial Number 3
#define FM33_SERIAL_NR_2	0x12	// Serial Number 2
#define FM33_SERIAL_NR_1	0x11	// Serial Number 1
#define FM33_SERIAL_NR_0	0x10	// Serial Number 0
#define FM33_EVENT_CNTR_1	0x0F	// Event Counter 1
#define FM33_EVENT_CNTR_0	0x0E	// Event Counter 0
#define FM33_EVENT_CNTR_CTRL	0x0D	// Event Counter Control
#define FM33_WATCHDOG_CTRL_1	0x0C	// Watchdog Control 1
#define FM33_WATCHDOG_CTRL_0	0x0B	// Watchdog Control 0
#define FM33_WATCHDOG_RESTART	0x0A	// Watchdog Restart
#define FM33_WATCHDOG_FLAGS	0x09	// Watchdog Flags
#define FM33_YEARS		0x08	// Years
#define FM33_MONTH		0x07	// Month
#define FM33_DATE		0x06	// Date
#define FM33_DAY		0x05	// Day
#define FM33_HOURS		0x04	// Hours
#define FM33_MINUTES		0x03	// Minutes
#define FM33_SECONDS		0x02	// Seconds
#define FM33_CAL_CTRL		0x01	// CAL/Control
#define FM33_RTC_ALARM_CTRL	0x00	// RTC_Alarm Control

/* Masks */
#define FM33_MASK_OSCEN		0x80	// OSCEN_n
#define FM33_MASK_CLKW		0x02	// RTC/Alarm Control W
#define FM33_MASK_CLKR		0x01	// RTC/Alarm Control R

struct fm33256b_clock {
	uint8_t seconds;
	uint8_t minutes;
	uint8_t hour;
	uint8_t day;
	uint8_t date;
	uint8_t month;
	uint8_t year;
} __attribute__ ((packed));


int fm33256b_write(uint8_t *in, int inlen);
int fm33256b_read(uint8_t *cmd, uint8_t cmdlen, uint8_t *out, int outlen);

/**
 * Write data to FRAM
 * @param addr address to write data at
 * @param data pointer to data
 * @param len number of bytes to write
 */
void fm33256b_write_data(uint16_t addr, uint8_t *data, uint16_t len);

/**
 * Read data from FRAM
 * @param addr address to read from
 * @param data pointer to buffer
 * @param len number of bytes to read
 */
void fm33256b_read_data(uint16_t addr, uint8_t *data, uint16_t len);

/**
 * Stop RTC clock
 * @return
 */
int fm33256b_clock_halt(void);
/**
 * Start RTC clock
 * @return
 */
int fm33256b_clock_resume(void);

/**
 * Setup spi for fm33256 and must be called before the other commands
 * @return always 0
 */
int fm33256b_init(void);

/**
 * Write full clock to RTC
 * @param clock fm33256b_clock struct with time
 * @return 0 on success, -1 on err
 */
int fm33256b_clock_write_burst(struct fm33256b_clock *clock);

/**
 * Read clock from RTC
 * @param clock pointer to fm33256b_clock struct
 * @return 0 on success, -1 on err
 */
int fm33256b_clock_read_burst(struct fm33256b_clock *clock);

/**
 * Convert from time_t struct to fm33256b_clock struct
 * @param time pointer to time_t
 * @param clock pointer to fm33256b_clock
 * @return 0 on success, -1 on err
 */
int fm33256b_time_to_clock(time_t *time, struct fm33256b_clock *clock);

/**
 * Convert from fm33256b_clock to time_t
 * @param time pointer to time_t
 * @param clock pointer to fm33256b_clock
 * @return 0 on success, -1 on err
 */
int fm33256b_clock_to_time(time_t *time, struct fm33256b_clock *clock);

void fm33256b_lock_upper_quarter(void);
void fm33256b_unlock_upper_quarter(void);

static inline void * fm33256b_write_data_memcpy(void * to, const void * from, size_t size) {
	fm33256b_write_data((uint16_t) (uintptr_t) to, (uint8_t *) from, (uint16_t) size);
	return to;
}

static inline void * fm33256b_read_data_memcpy(void * to, const void * from, size_t size) {
	fm33256b_read_data((uint16_t) (uintptr_t) from, (uint8_t *) to, (uint16_t) size);
	return to;
}

void cmd_rtc_setup(void);
#endif // _FM33256B_H_
