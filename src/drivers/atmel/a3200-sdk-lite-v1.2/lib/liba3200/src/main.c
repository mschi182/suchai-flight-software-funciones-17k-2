/**
 * NanoMind firmware
 *
 * @author Johan De Claville Christiansena
 * @author Morten Jensen
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <stdio.h>

#include <conf_a3200.h>

#include <sysclk.h>
#include <wdt.h>
#include <gpio.h>
#include <sdramc.h>
#include <reset_cause.h>

#include <dev/usart.h>
#include <dev/i2c.h>
#include <dev/cpu.h>

#include <FreeRTOS.h>
#include <task.h>

#include <util/vmem.h>
#include <fm33256b.h>

#include "led.h"
#include "pwr_switch.h"
#include <fm33256b.h>

/* Setup VMEM compile time */
const vmem_t vmem_map[] = {
		{.name = "FRAM",  .start = (vmemptr_t) 0x10000000, .physmem_start = (vmemptr_t) 0x0, .length = 1024 * 32, .write_mem = fm33256b_write_data_memcpy, .read_mem = fm33256b_read_data_memcpy},
		{}, //! Sentinel value: Do not remove
};

int main() {

	/* Reset watchdog */
	wdt_disable();
	wdt_clear();
	wdt_opt_t wdtopt = { .us_timeout_period = 5000000 };
	wdt_enable(&wdtopt);

	/* Initialize interrupt handling.
	 * This function call is needed, when using libasf startup.S file */
	INTC_init_interrupts();

	/* Init CLK */
	sysclk_init();

	/* Allow custom main hook */
	extern void hook_main(void);
	hook_main();

	/* Init pwr channels */
	pwr_switch_init();

	/* Init SDRAM, do this before malloc is called */
	sdramc_init(sysclk_get_cpu_hz());

	/* We have to trust that the SDRAM is working by now */
	extern void *heap_start, *heap_end;
	heap_start = (void *) 0xD0000000 + 0x100000;
	heap_end = (void *) 0xD0000000 + 0x2000000 - 1000;

	/* Init USART for debugging */
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);
	static const gpio_map_t USART_GPIO_MAP = {
		{USART_RXD_PIN, USART_RXD_FUNCTION},
		{USART_TXD_PIN, USART_TXD_FUNCTION},
	};
	gpio_enable_module(USART_GPIO_MAP, sizeof(USART_GPIO_MAP) / sizeof(USART_GPIO_MAP[0]));
	usart_init(USART_CONSOLE, sysclk_get_peripheral_bus_hz(USART), 500000);
	usart_stdio_id = USART_CONSOLE;

	/* Init LED */
	led_init();

	/* Start init task at highest priority */
	void init_task(void * param);
	xTaskCreate(init_task, "INIT", 4000, NULL, configMAX_PRIORITIES - 1, NULL);

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	while(1) cpu_reset();

}
