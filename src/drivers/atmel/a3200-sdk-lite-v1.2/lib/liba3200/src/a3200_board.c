/**
 * NanoMind A32000 firmware
 *
 * @author Morten Jensen 
 * Copyright 2014 GomSpace ApS. All rights reserved.
 */

#include <param/param.h>
#include <pwr_switch.h>
#include <mb_switch.h>
#include <adc_channels.h>
#include <a3200_board.h>
#include <conf_liba3200.h>

void a3200_board_callback(uint16_t addr, param_index_t * mem) {

	switch(addr) {
		case A3200_PWR_GSSB1: {
			uint8_t state = param_get_uint8(A3200_PWR_GSSB1, A3200_BOARD);
			if (state)
				pwr_switch_enable(PWR_GSSB);
			else
				pwr_switch_disable(PWR_GSSB);
			break;
		}
		case A3200_PWR_GSSB2: {
			uint8_t state = param_get_uint8(A3200_PWR_GSSB2, A3200_BOARD);
			if (state)
				pwr_switch_enable(PWR_GSSB2);
			else
				pwr_switch_disable(PWR_GSSB2);
			break;
		}

		case A3200_PWR_FLASH: {
			uint8_t state = param_get_uint8(A3200_PWR_FLASH, A3200_BOARD);
			if (state)
				pwr_switch_enable(PWR_SD);
			else
				pwr_switch_disable(PWR_SD);
			break;
		}

#if BOARDREV >= 3
		case A3200_PWR_PWM: {
			uint8_t state = param_get_uint8(A3200_PWR_PWM, A3200_BOARD);
			if (state)
				pwr_switch_enable(PWR_PWM);
			else
				pwr_switch_disable(PWR_PWM);
			break;
		}
#if MBSWITCH_ENABLED
		case A3200_PWR_GPS: {
			uint8_t state = param_get_uint8(A3200_PWR_GPS, A3200_BOARD);
			if (state)
				mb_switch_enable(MB_SWITCH_GPS);
			else
				mb_switch_disable(MB_SWITCH_GPS);
			break;
		}

		case A3200_PWR_WDE: {
			uint8_t state = param_get_uint8(A3200_PWR_WDE, A3200_BOARD);
			if (state)
				mb_switch_enable(MB_SWITCH_WDE);
			else
				mb_switch_disable(MB_SWITCH_WDE);
			break;
		}
#endif
#endif

	}

}
