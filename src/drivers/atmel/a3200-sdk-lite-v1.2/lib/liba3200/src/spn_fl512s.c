#include <inttypes.h>
#include <spi.h>
#include <sysclk.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include <util/log.h>
#include <util/driver_debug.h>

#include "pwr_switch.h"
#include "spn_fl512s.h"
#include "board.h"

SemaphoreHandle_t spn_fl512s_lock = NULL;
static int current_cs = SPN_FL512S_CS0;

uint8_t spn_fl512s_init(unsigned int core_id) {

	switch(core_id) {
	case 0:
		current_cs = SPN_FL512S_CS0;
		break;
	case 1:
		current_cs = SPN_FL512S_CS1;
		break;
	default:
		log_error("Invalid core id");
		return SPN_INV_CORE;
		break;
	}

	spn_fl512s_lock = xSemaphoreCreateMutex();
	if (spn_fl512s_lock == NULL)
		return SPN_NOMEM;

	/* Turn on power */
	pwr_switch_enable(PWR_SD);

	/* Spi options */
	spi_options_t spi_opt = {};
	spi_opt.modfdis = 1;
	spi_opt.baudrate = 8000000;
	spi_opt.bits = 8;
	spi_opt.spi_mode = 0;
	spi_opt.stay_act = 1;
	spi_opt.trans_delay = 0;
	spi_opt.reg = current_cs;

	/* Setup chip */
	int result = spi_setupChipReg(SPN_Fl512S_SPI, &spi_opt, sysclk_get_peripheral_bus_hz(SPN_Fl512S_SPI));
	if (result != SPI_OK) {
		log_error("SPI setup failed");
		return SPN_SPI_SETUP_FAIL;
	} else {
		return SPN_NO_ERR;
	}
}

static uint8_t spn_fl512s_read_status_register_no_lock() {
	uint16_t data;

	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	spi_write(SPN_Fl512S_SPI, SPN_FL512S_CMD_RDSR);
	spi_write(SPN_Fl512S_SPI, 0xFF);
	spi_read(SPN_Fl512S_SPI, &data);
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);
	return (uint8_t) data;
}

uint8_t spn_fl512s_read_status_register() {
	xSemaphoreTake(spn_fl512s_lock, 5000);
	uint8_t retval = spn_fl512s_read_status_register_no_lock();
	xSemaphoreGive(spn_fl512s_lock);
	return retval;
}

void spn_fl512s_read_device_id(char data[64]) {
	xSemaphoreTake(spn_fl512s_lock, 5000);
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	spi_write(SPN_Fl512S_SPI, SPN_FL512S_CMD_RDID);
	for(int i = 0; i < 64; i++) {
		uint16_t read;
		spi_write(SPN_Fl512S_SPI, 0xff);
		spi_read(SPN_Fl512S_SPI, &read);
		data[i] = (uint8_t) read;
	}
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);
	xSemaphoreGive(spn_fl512s_lock);
}

void spn_fl512s_read_data(uint32_t addr, uint8_t *data, uint16_t len) {
	driver_debug(DEBUG_ISI, "SPN READ %"PRIu32" %"PRIu16"\r\n", addr, len);
	xSemaphoreTake(spn_fl512s_lock, 5000);
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	uint8_t cmd[5] = {SPN_FL512S_CMD_READ, (addr >> 24) & 0xFF, (addr >> 16) & 0xFF, (addr >> 8) & 0xFF, addr & 0xFF};
	for(int i = 0; i < 5; i++)
		spi_write(SPN_Fl512S_SPI, cmd[i]);

	for(int i = 0; i < len; i++) {
		uint16_t read;
		spi_write(SPN_Fl512S_SPI, 0xff);
		spi_read(SPN_Fl512S_SPI, &read);
		data[i] = (uint8_t) read;
	}
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);
	xSemaphoreGive(spn_fl512s_lock);
}

uint8_t spn_fl512s_write_data(uint32_t addr, uint8_t *data, uint16_t len) {

	driver_debug(DEBUG_ISI, "SPN WRITE %"PRIu32" %"PRIu16"\r\n", addr, len);
	xSemaphoreTake(spn_fl512s_lock, 5000);

	/* Operation1: Enable writes */
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	spi_write(SPN_Fl512S_SPI, SPN_FL512S_CMD_WREN);
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);

	/* Operation2: Write */
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	uint8_t cmd[5] = {SPN_FL512S_CMD_PP, (addr >> 24) & 0xFF, (addr >> 16) & 0xFF, (addr >> 8) & 0xFF, addr & 0xFF};
	for(int i = 0; i < 5; i++)
			spi_write(SPN_Fl512S_SPI, cmd[i]);
	for(int i = 0; i < len; i++) {
		spi_write(SPN_Fl512S_SPI, data[i]);
	}
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);

	portTickType start_time;
	start_time = xTaskGetTickCount();

	/* Opreation3: Poll Write-In-Progress */
	while((spn_fl512s_read_status_register_no_lock() & 0x01) && (xTaskGetTickCount() < (start_time + SPN_WRITE_TO))) {
		taskYIELD();
	}

	xSemaphoreGive(spn_fl512s_lock);

	if (xTaskGetTickCount() >= (start_time + SPN_WRITE_TO))
		return SPN_TIMEOUT;
	else
		return SPN_NO_ERR;
}

uint8_t spn_fl512s_erase_block(uint32_t addr) {

	driver_debug(DEBUG_ISI, "SPN WRITE %"PRIu32"\r\n", addr);
	xSemaphoreTake(spn_fl512s_lock, 5000);

	/* Operation1: Enable writes */
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	spi_write(SPN_Fl512S_SPI, SPN_FL512S_CMD_WREN);
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);

	/* Opreation2: Send erase cmd */
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	uint8_t cmd[5] = {SPN_FL512S_CMD_SE, (addr >> 24) & 0xFF, (addr >> 16) & 0xFF, (addr >> 8) & 0xFF, addr & 0xFF};
	for(int i = 0; i < 5; i++)
		spi_write(SPN_Fl512S_SPI, cmd[i]);
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);

	int32_t timer = SPN_ERASEB_TO; // TImeout

	/* Opreation3: Poll Write-In-Progress */
	while((spn_fl512s_read_status_register_no_lock() & 0x01) && (timer > 0)) {
		vTaskDelay(1);
		timer -= 1;
	}

	xSemaphoreGive(spn_fl512s_lock);

	if (timer < 1)
		return SPN_TIMEOUT;
	else
		return SPN_NO_ERR;
}

uint8_t spn_fl512s_erase_chip(void) {

	xSemaphoreTake(spn_fl512s_lock, 5000);

	/* Operation1: Enable writes */
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	spi_write(SPN_Fl512S_SPI, SPN_FL512S_CMD_WREN);
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);

	/* Opreation2: Send erase cmd */
	spi_selectChip(SPN_Fl512S_SPI, current_cs);
	spi_write(SPN_Fl512S_SPI, SPN_FL512S_CMD_BE);
	spi_unselectChip(SPN_Fl512S_SPI, current_cs);

	int32_t timer = SPN_ERASEC_TO; // Timeout

	/* Opreation3: Poll Write-In-Progress */
	while((spn_fl512s_read_status_register_no_lock() & 0x01) && (timer > 0)) {
		vTaskDelay(1);
		timer -= 1;
	}

	xSemaphoreGive(spn_fl512s_lock);

	if (timer < 1)
		return SPN_TIMEOUT;
	else
		return SPN_NO_ERR;
}

